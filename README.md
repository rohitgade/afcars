# NCANDS

Refered Link:
* Data Set :
    [About](https://www.ndacan.acf.hhs.gov/datasets/dataset-details.cfm?ID=225)
* Random Forest :
    [Link 1](https://towardsdatascience.com/random-forest-in-python-24d0893d51c0)
    [Link 2](https://www.kaggle.com/raviolli77/random-forest-in-python)
* Handling Inbalanced Dataset :
     [Link 1](https://machinelearningmastery.com/tactics-to-combat-imbalanced-classes-in-your-machine-learning-dataset/)
     [Link 2](https://www.analyticsvidhya.com/blog/2017/03/imbalanced-classification-problem/)
* Resampling :
     [Link 1](https://www.kaggle.com/rafjaa/resampling-strategies-for-imbalanced-datasets#t3)
     [Link 2](https://www.kaggle.com/rafjaa/resampling-strategies-for-imbalanced-datasets#t3)
* Comparing Different Models :
     [Link 1](https://towardsdatascience.com/comparing-different-classification-machine-learning-models-for-an-imbalanced-dataset-fdae1af3677f)
     [Link 2]()
* Others :
     [Cross Validation](https://stackoverflow.com/questions/38151615/specific-cross-validation-with-random-forest)

